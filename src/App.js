import React, {Component} from 'react';
import './App.css';
import Button from "./components/button/button";
import Show from "./components/showchildren/show";

class App extends Component {
  state={
    joke: {}
  };

  joke = () =>{
    fetch('https://api.chucknorris.io/jokes/random').then(response =>{
      if (response.ok){
        return response.json()
      }
      throw new Error('Something wrong with request');
    }).then(post =>{
      const UpdateJoke = {value: post.value};
      this.setState({joke: UpdateJoke});
    }).catch(error =>{
      console.log(error);
    })
  };

  componentDidMount() {
    this.joke();
  }

  render() {
    return (
      <div className="App">
        <Button
           joke={()=> this.joke()}
        />
        <Show>
          {this.state.joke.value}
        </Show>
      </div>
  );
  }


}

export default App;
