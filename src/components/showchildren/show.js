import React, {Component} from 'react';
import './show.css'
class Show extends Component {
    render() {
        return (
            <article>
                <p className='Post'>{this.props.children}</p>
            </article>
        );
    }
}

export default Show;