import React, {Component} from 'react';
import './button.css'

class Button extends Component {
    render() {
        return (
            <button className='button' onClick={this.props.joke}>Get joke to me</button>
        );
    }
}

export default Button;